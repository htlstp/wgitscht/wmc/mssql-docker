## Dockerized Version of MSSQL Server
this is an image for local testing only. You can (but don't need to) override the default pwd by setting the env variables
 when you start the container, or while building it. Careful: your pwd needs to comply with the MS pwd [rules](https://learn.microsoft.com/en-us/sql/relational-databases/security/password-policy?view=sql-server-ver16) To override e.g. SA_PASSWORD with a new value, you can pass the new value as an argument when running the container: 

 ```docker run -e SA_PASSWORD=new_value```


### prerequisits
you need docker desktop for windows, or docker for linux/macos.
for windows install via winget

```winget install Docker.DockerDesktop```

for linux use your favorite package manager e.g.

```pacman -S docker```

### howto build
change to the docker directory and issue 

```docker build . -t wmc-mssql```

you can change the tag to whatever suits your needs, you can also tag a version 
e.g. wmc-mssql:v1

### howto run
open a cmd shell and run:

```docker run -p 1433:1433 --name mssql --hostname mssql -d wmc-mssql:latest```

you need to adopt this command if you chose a different name (tag). If you choose different pwds you can set 
the env vars SA_PASSWORD and WMC_USER_PASSWORD by using ```-e SA_PASSWORD=<my-pwd>``` and ```-e WMC_USER_PASSWORD=<my-wmc-pwd>``` respectively.

### howto configure
if you want different or more tables created take a look at the initialize-db.sh script, which creates the user, a database, some tables and adds some content.



