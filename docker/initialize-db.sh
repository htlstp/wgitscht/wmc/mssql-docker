sleep 60s
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P ${SA_PASSWORD} -d master -i setup-db.sql
# add your scripts here (db is set up)- we use the wmc_user from now on!!
/opt/mssql-tools/bin/sqlcmd -S localhost -U wmc_user -P ${WMC_USER_PASSWORD} -d wmc -i setup-tables.sql
/opt/mssql-tools/bin/sqlcmd -S localhost -U wmc_user -P ${WMC_USER_PASSWORD} -d wmc -i user-data.sql
