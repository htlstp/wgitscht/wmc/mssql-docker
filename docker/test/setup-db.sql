IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = 'wmc')
BEGIN
    CREATE DATABASE wmc;
END
GO

DECLARE @WMC_USER_PASSWORD NVARCHAR(100);
SET @WMC_USER_PASSWORD = '$(WMC_USER_PASSWORD)';

IF NOT EXISTS (SELECT name FROM sys.database_principals WHERE name = 'wmc_user')
BEGIN
    USE wmc;
    
    DECLARE @sql NVARCHAR(MAX);
    SET @sql = 'CREATE LOGIN wmc_user WITH PASSWORD = ' + QUOTENAME(@WMC_USER_PASSWORD, '''') + ', CHECK_POLICY = OFF;';    
    EXEC sp_executesql @sql;

    CREATE USER wmc_user FOR LOGIN wmc_user WITH DEFAULT_SCHEMA = dbo;
    ALTER ROLE db_owner ADD MEMBER wmc_user;    
END
GO